import { Context } from 'vm'

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

export default {
  /** Build configuration */
  build: {
    extend (config: any, context: Context) {
    },
    babel: {
      presets({ isServer }:any) {
        return [
          [
            require.resolve('@nuxt/babel-preset-app'),
            // require.resolve('@nuxt/babel-preset-app-edge'), // For nuxt-edge users
            {
              buildTarget: isServer ? 'server' : 'client',
              corejs: { version: 3 }
            }
          ]
        ]
      }
    },
    loaders: {
      // we want to use sass instead of node-sass
      sass: {
        import: ['~assets/style/app.sass'],
        implementation: require('sass'),
        fiber: require('fibers')
      }
    },
    plugins: [new VuetifyLoaderPlugin()],
    transpile: ['vuetify/lib'],
    typescript: {
      // this is required - if set to true the HMR in dev will time out
      typeCheck: false
    }
  },
  /** @see https://typescript.nuxtjs.org/migration.html */
  buildModules: ['@nuxt/typescript-build'],
  /** Plugins to load before mounting the App **/
  plugins: [
    '~/plugins/vuetify'
  ],
  modules: [
    [
      'nuxt-fire',
      {
        config: {
          apiKey: "AIzaSyBWfZawbfvHW8TDd8tDo9bmtbbe77qhP4U",
          authDomain: "she-stay-project.firebaseapp.com",
          databaseURL: "https://she-stay-project.firebaseio.com",
          projectId: "she-stay-project",
          storageBucket: "she-stay-project.appspot.com",
          messagingSenderId: "728962078927",
          appId: "1:728962078927:web:76229262c079fcb6687fba",
          measurementId: "G-EEE1S8F7LE"
        },
        services: {
          firestore: true
        }
      }
    ]
  ],
  /** typescript config for nuxt */
  typescript: {
    typeCheck: false,
    ignoreNotFoundWarnings: true
  }
}
